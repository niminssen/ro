#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>
#include <fstream>
using namespace std;
#include "glpk.h"

void main(void)
{
	//les constantes
	int max_int = 1000;
	int n = 0;
	int m = 0;
	int temp = 0;
	const int MAX = 200;

	int M[MAX][MAX];
	int P[MAX][MAX];

	std::ifstream file("la01.txt");

	if (file)
	{
		file >> n;
		file >> m;

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				file >> temp;
				temp++;
				M[i][j] = temp;
				file >> P[i][j];
			}
		}
		file.close();
	}
	else cout << "erreur de lecture du fichier\n";

	//affichafe instance 
	cout << "+++ M +++" << endl;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++)
			cout << M[i][j] << " ";
		cout << endl;
	}
	cout << "+++ P +++" << endl;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++)
			cout << P[i][j] << " ";
		cout << endl;
	}

	// verification solveur
	std::cout << "GLPK version: " << glp_version() << endl;

	// creation des tableaux pour le solveur
	glp_prob *lp;
	int ia[1 + 10000], ja[1 + 10000];
	double ar[1 + 10000], z, x1, x2;

	// definition du probleme
	lp = glp_create_prob();
	glp_set_prob_name(lp, "Jobshop");
	glp_set_obj_dir(lp, GLP_MIN);

	// variable globales pour la generation
	int CompteurContrainte = 0;
	int CompteurCol = 0;
	int CompteurIA = 0;
	
	string nomcol;
	string nomcontrainte;
	int position;

	// definition des variables - Slides 45
	for (int i = 1; i <= n; i++)  // st = starting time
	{
		for (int j = 1; j <= m; j++)
		{
			std::ostringstream oss;
			oss << "st_" << i << '_' << j;
			nomcol = oss.str();
			glp_add_cols(lp,1);
			CompteurCol = CompteurCol + 1;
			glp_set_col_name(lp, CompteurCol, nomcol.c_str());
			glp_set_col_kind(lp, CompteurCol, GLP_IV);
			glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
		}
	}
	for (int i = 1; i <= n; i++)
		{
		for (int j = 1; j <= m; j++)
		{
			for (int k = 1; k <= n; k++)
			{
				for (int p = 1; p <= n; p++)
				{
					if (i != k && M[i][j] == M[k][p])
					{
						std::ostringstream oss;
						oss << "b_" << i <<"_" << j << "_" << k <<"_" << p;
						nomcol = oss.str();
						glp_add_cols(lp, 1);
						CompteurCol = CompteurCol + 1;
						glp_set_col_name(lp, CompteurCol, nomcol.c_str());
						glp_set_col_kind(lp, CompteurCol, GLP_BV);
						glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
					}
				}
			}
		}
		}

	// creation Index pour les recherches
	glp_create_index(lp);

	// containte sur les starting times
	// -----------
	for (int i = 1; i <= n; i++)
	{
		for (int j = 2; j <=m; j++)
		{
			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, P[i][j-1], P[i][j - 1]);

			//std::ostringstream oss;
			oss.str("");
			oss << "st_" << i <<"_" << j;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = 1.0;

			//std::ostringstream oss;
			oss.str("");
			oss << "st_" << i << "_" << j-1;
			nomcol = oss.str();

			position = glp_find_col(lp, nomcol.c_str());
			CompteurIA = CompteurIA + 1;
			ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = -1.0;
		}
	}

	glp_load_matrix(lp, CompteurIA, ia, ja, ar);

	glp_write_lp(lp, NULL, "partie1.lp");
	
	// contraintes sur les disjonctions

	for (int i = 1; i <= n ; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			for (int k = 1; k <= n; k++)
			{
				for (int p = 1; p <= m; p++)
				{
					if (i != k && M[i][j] == M[k][p]) {

						 //premiere partie contrainte

						glp_add_rows(lp, 1);
						CompteurContrainte++;

						std::ostringstream oss;
						oss << "C_" << CompteurContrainte << "_" << 1;
						nomcontrainte = oss.str();

						glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
						glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, P[k][p] - max_int, P[k][p] - max_int);

						//std::ostringstream oss;
						oss.str("");
						oss.clear();
						oss << "st_" << i << "_" << j;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = 1.0;

						//std::ostringstream oss;
						oss.str("");
						oss.clear();
						oss << "st_" << k << "_" << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = -1.0;

						//std::ostringstream oss;
						oss.str("");
						oss.clear();
						oss << "b_" << i << "_" << j << "_" << k << "_" << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = -max_int;

						// deuxieme partie contrainte
						
						glp_add_rows(lp, 1);
						CompteurContrainte++;

						oss.str("");
						oss.clear();
						oss << "C_" << CompteurContrainte;
						nomcontrainte = oss.str();

						glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
						glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, P[i][j], P[i][j]);

						//std::ostringstream oss;
						oss.str("");
						oss.clear();
						oss << "st_" << k << "_" << p;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = 1.0;

						//std::ostringstream oss;
						oss.str("");
						oss.clear();
						oss << "st_" << i << "_" << j;
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = -1.0;

						//std::ostringstream oss;
						oss.str("");
						oss.clear();
						oss << "b_" << i << "_" << j << "_" << k << "_" << p;
						cout << oss.str() << "\n";
						nomcol = oss.str();

						position = glp_find_col(lp, nomcol.c_str());
						CompteurIA = CompteurIA + 1;
						ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = max_int;
						
					}
				}
			}
		}
	}
	glp_load_matrix(lp, CompteurIA, ia, ja, ar);

	glp_write_lp(lp, NULL, "partie1.lp");

	// fonction objectif

	glp_add_cols(lp, 1);
	CompteurCol++;
	glp_set_col_name(lp, CompteurCol, "z");
	glp_set_col_kind(lp, CompteurCol, GLP_IV);
	glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0, 0);
	//glp_create_index(lp);

	for (int i = 1; i <= n; i++) {

		std::ostringstream oss;
		CompteurContrainte++;
		oss.str("");
		oss << "c_" << CompteurContrainte;
		glp_add_rows(lp, 1);
		glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
		glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, P[i][m], 0);

		oss.str("");
		oss << "st_" << i << '_' << m;
		position = glp_find_col(lp, oss.str().c_str());
		CompteurIA++;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = -1;

		position = glp_find_col(lp, "z");
		CompteurIA++;
		ia[CompteurIA] = CompteurContrainte;
		ja[CompteurIA] = position;
		ar[CompteurIA] = 1;
	}

	// les coefficients de la fonction objectif
	std::ostringstream oss;
	oss.str("");
	oss << "z";
	nomcol = oss.str();
	position = glp_find_col(lp, nomcol.c_str());
	glp_set_obj_coef(lp, position, 1);


	glp_load_matrix(lp, CompteurIA, ia, ja, ar);

	glp_write_lp(lp, NULL, "partie1.lp");

	glp_simplex(lp, NULL);
	glp_intopt(lp, NULL);


	// valeur de la fonction objectif
	//z = glp_mip_obj_val(lp);
	z = glp_get_obj_val(lp);
	std::cout << "z = " << z << endl;

	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
		{
			std::ostringstream oss;
			oss << "st_" << i << '_' << j;
			nomcol = oss.str();
			position = glp_find_col(lp, nomcol.c_str());
			int d = glp_mip_col_val(lp, position);
			cout << "st_" << i << j << " : " << d << endl;
		}
	}


	glp_delete_prob(lp);
	glp_free_env();


	
}