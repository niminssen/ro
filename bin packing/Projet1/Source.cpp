#include <stdio.h>
#include <stdlib.h>
#include <sstream> 
#include <iostream>
#include <fstream>
#include <vector>
using namespace std;
#include "glpk.h"

void main(void)
{
	bool stop = 0;
	//les constantes
	int Ws = 100; // stock width
	int W[5] = {0 ,45 ,36 ,31 ,14 }; // items width
	int D[5] = { 0,97,610,395,211 }; // demande des differents items
	std::vector< std::vector<int>> Patterns = { {0,0,0,0,0},{0,1,0,0,0},{0,0,1,0,0},{0,0,0,1,0},{0,0,0,0,1} };
	int nbItems = 4;
	int nbPatterns;
	std::vector<float> V;
	std::vector<int> U;
	int compteur = 0;
	std::vector<int> Solution;


	while (stop != 1 && compteur <= 10)
	{
		//*************************************MASTER PROBLEM*********************************//

		nbPatterns = Patterns.size() - 1;
		cout <<"le nombre de patterns est de : " << nbPatterns << endl;
		// verification solveur
		std::cout << "GLPK version: " << glp_version() << endl;

		// creation des tableaux pour le solveur
		glp_prob* lp;
		int ia[1 + 10000], ja[1 + 10000];
		double ar[1 + 10000], z;

		// definition du probleme
		lp = glp_create_prob();
		glp_set_prob_name(lp, "master problem");
		glp_set_obj_dir(lp, GLP_MIN);

		// variable globales pour la generation
		int CompteurContrainte = 0;
		int CompteurCol = 0;
		int CompteurIA = 0;

		string nomcol;
		string nomcontrainte;
		int position;

		// definition des variables - Slides 45
		for (int j = 1; j <= nbPatterns; j++)  // nb of boards cut accordiang to the pattern j
		{
			std::ostringstream oss;
			oss << "x_" << j;
			nomcol = oss.str();
			glp_add_cols(lp, 1);
			CompteurCol = CompteurCol + 1;
			glp_set_col_name(lp, CompteurCol, nomcol.c_str());
			glp_set_col_kind(lp, CompteurCol, GLP_IV);
			glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
		}


		// creation Index pour les recherches
		glp_create_index(lp);

		// containte 
		// -----------
		for (int i = 1; i <= nbItems; i++)
		{
			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, D[i], D[i]);

			for (int p = 1; p <= nbPatterns; p++)
			{

				//std::ostringstream oss;
				oss.str("");
				oss << "x_" << p;
				nomcol = oss.str();

				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = Patterns[p][i];

			}
		}

		glp_load_matrix(lp, CompteurIA, ia, ja, ar);

		glp_write_lp(lp, NULL, "partie1.lp");


		// fonction objectif
		{
			glp_add_cols(lp, 1);
			CompteurCol++;
			glp_set_col_name(lp, CompteurCol, "z");
			glp_set_col_kind(lp, CompteurCol, GLP_IV);
			glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0, 0);
			//glp_create_index(lp);

			std::ostringstream oss;
			CompteurContrainte++;
			oss.str("");
			oss << "c_" << CompteurContrainte;
			glp_add_rows(lp, 1);
			glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_LO, 0, 0);


			position = glp_find_col(lp, "z");
			CompteurIA++;
			ia[CompteurIA] = CompteurContrainte;
			ja[CompteurIA] = position;
			ar[CompteurIA] = 1;

			for (int p = 1; p <= nbPatterns; p++) {

				oss.str("");
				oss << "x_" << p;
				position = glp_find_col(lp, oss.str().c_str());
				CompteurIA++;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = -1;
			}

		}

		{
			// les coefficients de la fonction objectif
			std::ostringstream oss;
			oss.str("");
			oss << "z";
			nomcol = oss.str();
			position = glp_find_col(lp, nomcol.c_str());
			glp_set_obj_coef(lp, position, 1);


			glp_load_matrix(lp, CompteurIA, ia, ja, ar);

			glp_write_lp(lp, NULL, "partie1.lp");

			glp_simplex(lp, NULL);
			glp_intopt(lp, NULL);
		}

		// valeur de la fonction objectif
		z = glp_mip_obj_val(lp);
		//z = glp_get_obj_val(lp);
		std::cout << "z = " << z << endl;

		V.push_back(0);
		V.push_back(glp_get_row_dual(lp, 1));
		V.push_back(glp_get_row_dual(lp, 2));
		V.push_back(glp_get_row_dual(lp, 3));
		V.push_back(glp_get_row_dual(lp, 4));


		//cout << cr1 << " " << cr2 << " " << cr3 << " " << cr4 << endl;

		Solution.clear();
		Solution.push_back(0);

		for (int i = 1; i <= nbPatterns; i++)
		{
			std::ostringstream oss;
			oss << "x_" << i;
			nomcol = oss.str();
			position = glp_find_col(lp, nomcol.c_str());
			int d = glp_mip_col_val(lp, position);
			cout << "x_" << i << " : " << d << endl;
			Solution.push_back(d);
		}



		glp_delete_prob(lp);
		glp_free_env();

		////***************************END OF MASTER PROBLEM**********************************//



		////***************************SUB PROBLEM**********************************************//
		{
			// definition du probleme
			lp = glp_create_prob();
			glp_set_prob_name(lp, "sup problem");
			glp_set_obj_dir(lp, GLP_MAX);

			// variable globales pour la generation
			int CompteurContrainte = 0;
			int CompteurCol = 0;
			int CompteurIA = 0;

			string nomcol;
			string nomcontrainte;
			int position;

			// definition des variables
			for (int i = 1; i <= nbItems; i++)  // nb de fois que la decoupe p est utilis�
			{
				std::ostringstream oss;
				oss << "u_" << i;
				nomcol = oss.str();
				glp_add_cols(lp, 1);
				CompteurCol = CompteurCol + 1;
				glp_set_col_name(lp, CompteurCol, nomcol.c_str());
				glp_set_col_kind(lp, CompteurCol, GLP_IV);
				glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0.0, 0.0);
			}

			// creation Index pour les recherches
			glp_create_index(lp);

			//// les vi : 


			////contrainte : le pattern generer doit etre valide

			glp_add_rows(lp, 1);
			CompteurContrainte++;

			std::ostringstream oss;
			oss.str("");
			oss << "C_" << CompteurContrainte;
			nomcontrainte = oss.str();

			glp_set_row_name(lp, CompteurContrainte, nomcontrainte.c_str());
			glp_set_row_bnds(lp, CompteurContrainte, GLP_UP, Ws, Ws);

			for (int i = 1; i <= nbItems; i++)
			{
				//std::ostringstream oss;
				oss.str("");
				oss << "u_" << i;
				nomcol = oss.str();
				position = glp_find_col(lp, nomcol.c_str());
				CompteurIA = CompteurIA + 1;
				ia[CompteurIA] = CompteurContrainte; ja[CompteurIA] = position; ar[CompteurIA] = W[i];

			}

			// fonction objectif
			{
				glp_add_cols(lp, 1);
				CompteurCol++;
				glp_set_col_name(lp, CompteurCol, "z");
				glp_set_col_kind(lp, CompteurCol, GLP_IV);
				glp_set_col_bnds(lp, CompteurCol, GLP_LO, 0, 0);
				//glp_create_index(lp);

				std::ostringstream oss;
				CompteurContrainte++;
				oss.str("");
				oss << "c_" << CompteurContrainte;
				glp_add_rows(lp, 1);
				glp_set_row_name(lp, CompteurContrainte, oss.str().c_str());
				glp_set_row_bnds(lp, CompteurContrainte, GLP_FX, 0, 0);


				position = glp_find_col(lp, "z");
				CompteurIA++;
				ia[CompteurIA] = CompteurContrainte;
				ja[CompteurIA] = position;
				ar[CompteurIA] = 1;

				for (int i = 1; i <= nbItems; i++) {

					oss.str("");
					oss << "u_" << i;
					position = glp_find_col(lp, oss.str().c_str());
					CompteurIA++;
					ia[CompteurIA] = CompteurContrainte;
					ja[CompteurIA] = position;
					ar[CompteurIA] = -V[i];
				}

			}

			{
				// les coefficients de la fonction objectif
				std::ostringstream oss;
				oss.str("");
				oss << "z";
				nomcol = oss.str();
				position = glp_find_col(lp, nomcol.c_str());
				glp_set_obj_coef(lp, position, 1);


				glp_load_matrix(lp, CompteurIA, ia, ja, ar);

				glp_write_lp(lp, NULL, "partie1.lp");

				glp_simplex(lp, NULL);
				glp_intopt(lp, NULL);
			}

			// valeur de la fonction objectif
			//z = glp_mip_obj_val(lp);
			z = glp_get_obj_val(lp);
			std::cout << "z = " << z << endl;
			if (z < 0) stop == 1;

			for (int i = 1; i <= nbItems; i++)
			{
				std::ostringstream oss;
				oss << "u_" << i;
				nomcol = oss.str();
				position = glp_find_col(lp, nomcol.c_str());
				int d = glp_mip_col_val(lp, position);
				cout << "u_" << i << " : " << d << endl;
			}
			// on enregistre le vecteur ui et on l'envoi dans la liste des patterns
			U.push_back(0);
			for (int i = 1; i <= nbItems; i++)
			{
				std::ostringstream oss;
				oss << "u_" << i;
				nomcol = oss.str();
				position = glp_find_col(lp, nomcol.c_str());
				int d = glp_mip_col_val(lp, position);
				U.push_back(d);
			}
			Patterns.push_back(U);

			U.clear();
			V.clear();

			glp_delete_prob(lp);
			glp_free_env();

			//******************************END SUB PROBLEM**************************////
		}
		compteur++;
	}

	for (size_t i = 1; i <= nbPatterns; ++i) {
		cout << "Il faut utiliser la decoupe suivante  : ";
		cout << Patterns[i][1] << "x" << W[1] << " ";
		cout << Patterns[i][2] << "x" << W[2] << " ";
		cout << Patterns[i][3] << "x" << W[3] << " ";
		cout << Patterns[i][4] << "x" << W[4] << " ";
		cout << " ";
		cout << Solution[i] << " fois";
		cout << std::endl;
	}
	cout << endl;

}
